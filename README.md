# Aide et Usage de ce service GITLab au CREMI

**Attention** : ce service est réservé aux usagers du CREMI.

## Authentification sur l'interface principale (HTML)

Vous devez utiliser le bouton **CREMI OpenID**, et vous pourrez vous connecter au serveur GITLab


## Mettre en Français ou autre localization.

Il faut aller dans [Préferences](https://gitlab.emi.u-bordeaux.fr/-/profile/preferences) et c'est tout en bas.


## Utiliser GIT et accèder à vos dêpots.

Du fait des limitations induites par l'usage de OpenID, vous pouvez accéder à vos dêpots qu'avec :

- [ssh et des clés dédiées ](https://gitlab.emi.u-bordeaux.fr/-/profile/keys)
- [Personal Access Tokens (Jetons d'accès)](https://gitlab.emi.u-bordeaux.fr/-/profile/personal_access_tokens) 

L'accès avec le couple Login/MDP via https ne fonctionne pas avec OpenID.

### ssh : après avoir copié votre clé ssh :
exemple pour un clone 
`git clone git@gitlab.emi.u-bordeaux.fr:le_chemin.git`

*  **Windows 10/11 : Usage client Natif Windows**   
Pour pouvoir utiliser le ssh-agent natif de windows, il faut utiliser le client ssh natif. Voici 2 façons de forcer cet usage :
    * Configure git :  `git config --global core.sshCommand "'C:\Windows\System32\OpenSSH\ssh.exe'"` 
    * Utiliser un variable d'Environement : : `set GIT_SSH=C:\WINDOWS\System32\OpenSSH\ssh.exe`


### Token : après avoir créé le token de projet ou d'utilisateur
exemple pour un clone 
`git clone https://<token-name>:<token>@gitlab.emi.u-bordeaux.fr:le_chemin.git`


## Projet Noté.

- Il est nécessaire d'inviter votre chargé de TD en tant que `Maintainer` dans votre Projet/Groupe.
- Ensuite le bon groupe d'observation sera ajouté par ses soins pour pouvoir évaluer votre projet. 




[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)
